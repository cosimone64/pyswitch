"""Author: Cosimo Agati

Packet module
This module contains a number of string constants to ease dictionary
field indexing.
"""


PKT_SRC = 'src'
PKT_DEST = 'dest'
PKT_PAYLOAD = 'payload'

RULE_NEXT_HOP = 0
RULE_METRIC = 1
RULE_INTERFACE = 2
RULE_IS_USED = 3

OP_TYPE = 'type'
OP_FUNCTION = 'op'
