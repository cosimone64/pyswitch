"""Author: Cosimo Agati

This module contains functions for generating:
    - policies;
    - blocking rules;
    - operations to be performed on packets.

They are INDEPENDENT from a specific router, and stateless.
"""


import indices
import customerrors

BLOCK_SRC = 0
BLOCK_DEST = 1
R_ONLY = R = 2
W_ONLY = W = 3

BLOCK_SRC_STR = 'block source'
BLOCK_DEST_STR = 'block dest'
READ_STR = 'r'
WRITE_STR = 'w'

FORWARDING_RULE_TYPE_ERROR = ('Incorrect types used to create a '
                              'forwarding rule')
BLOCKING_RULE_EXCEPTION_MSG = 'Invalid blocking rule type'
TOO_MANY_PAYLOADS_MSG = 'Only one payload string allowed in write operation'
UNSPECIFIED_PAYLOAD_MSG = 'Unspecified payload in write operation'
NOT_STRING_PAYLOAD_MSG = 'Specified payload is not a string'
INVALID_OPERATION_MSG = 'Invalid operation type'
PACKET_ERROR_MSG = 'All arguments should be str instances'


def get_forwarding_rule(dest, next_hop, metric, interface):
    """Returns a new forwarding rule as a dictionary.
    The parameters must be of type:
        dest: str
        next_hop: str
        metric: int
        interface: int
    """
    if not (isinstance(dest, str)
            and isinstance(next_hop, str)
            and isinstance(metric, int)
            and isinstance(interface, int)):
        raise TypeError(FORWARDING_RULE_TYPE_ERROR)
    return {dest: [next_hop, metric, interface, True]}


def get_policy(policy_type):
    """Returns a function to check if operations satisfy policies.
    The function checks if the 'type' field of the operation object
    is equal to the argument specified.
    """
    constraint = None
    if policy_type in (R, R_ONLY, 'read only'):
        constraint = 'r'
    elif policy_type in (W, W_ONLY, 'write only'):
        constraint = 'w'
    return lambda op: op[indices.OP_TYPE] != constraint


def get_blocking_rule(rule_type, addr):
    """Returns a function containing a blocking rule.
    The blocking rule checks the packet's specified field, and returns
    True if it can be sent, False otherwise.
    """
    if rule_type in (BLOCK_SRC_STR, BLOCK_SRC):
        return lambda pkt: pkt[indices.PKT_SRC] != addr
    elif rule_type in (BLOCK_DEST_STR, BLOCK_DEST):
        return lambda pkt: pkt[indices.PKT_DEST] != addr
    else:
        raise customerrors.BadCommandException(BLOCKING_RULE_EXCEPTION_MSG)


def get_operation(op_type, *payloads):
    """Returns an operation to be performed on a packet.
    Operations are dictionaries containing two entries:
        - type: a string specifying the operation type;
        - op:   a function (packet -> packet) to process a packet.
    """
    if op_type in (READ_STR, R):
        return {indices.OP_TYPE: 'r',
                indices.OP_FUNCTION: lambda pkt: pkt}
    elif op_type in (WRITE_STR, W):
        try:
            payload = payloads[0]
        except IndexError:
            raise TypeError(UNSPECIFIED_PAYLOAD_MSG)
        if len(payloads) != 1:
            raise TypeError(TOO_MANY_PAYLOADS_MSG)
        if not isinstance(payload, str):
            raise TypeError(NOT_STRING_PAYLOAD_MSG)

        def aux(pkt):
            """Auxiliary function to modify packet."""
            pkt[indices.PKT_PAYLOAD] = payload
            return pkt
        return {indices.OP_TYPE: 'w',
                indices.OP_FUNCTION: aux}
    else:
        raise customerrors.BadCommandException(INVALID_OPERATION_MSG)


def create_packet(src, dest, payload):
    """Creates a packet, represented as a dictionary."""
    if not (isinstance(src, str)
            and isinstance(dest, str)
            and isinstance(payload, str)):
        raise TypeError(PACKET_ERROR_MSG)
    return {indices.PKT_SRC: src,
            indices.PKT_DEST: dest,
            indices.PKT_PAYLOAD: payload}
