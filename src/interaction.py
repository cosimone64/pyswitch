#!/usr/bin/python3

"""Author: Cosimo Agati

This module shows a simulation of the interaction
between a router (switch) and the controller.
"""


import indices
import controller
import router
import customerrors


def main():
    """Main function."""
    # Creating a Router object
    my_router = router.Router(0, 1, 2, 3)
    # Creating a rule
    rule0 = controller.get_forwarding_rule('192.168.1.3',
                                           '192.168.1.1', 4, 1)
    # Adding a rule
    my_router.add_forwarding_rule(rule0)
    # Creating a packet
    pkt0 = controller.create_packet('192.168.1.4',
                                    '192.168.1.3',
                                    'AP sure is fun!')
    # Routing the packet
    print(parse_route(*my_router.route_packet(pkt0)))

    pkt1 = controller.create_packet('192.168.1.4',
                                    '192.168.1.5',
                                    'AP sure is fun!')
    # Routing a packet with destination not in table
    try:
        print(parse_route(*my_router.route_packet(pkt1)))
    except customerrors.RoutingException as msg:
        print(msg)

    my_router.drop_forwarding_rule('192.168.1.3')
    try:
        print(parse_route(*my_router.route_packet(pkt0)))
    except customerrors.RoutingException as msg:
        print(msg)

    # Re-adding the previously dropped rule
    my_router.add_forwarding_rule(rule0)
    print(parse_route(*my_router.route_packet(pkt0)))
    # Deleting a rule
    my_router.delete_forwarding_rule('192.168.1.3')
    try:
        print(parse_route(*my_router.route_packet(pkt0)))
    except customerrors.RoutingException as msg:
        print(msg)

    # Adding an read operation and a write operation
    op0 = controller.get_operation('r')
    op1 = controller.get_operation('w', 'Modified payload')
    my_router.add_operation(op0)
    my_router.add_operation(op1)
    my_router.add_forwarding_rule(rule0)
    print(parse_route(*my_router.route_packet(pkt0)))

    # Adding a blocking rule to block traffic from a source
    blocking_rule0 = controller.get_blocking_rule(controller.BLOCK_SRC,
                                                  '192.168.1.4')
    my_router.add_blocking_rule(blocking_rule0)
    try:
        print(parse_route(*my_router.route_packet(pkt0)))
    except customerrors.RoutingException as msg:
        print(msg)

    # Adding a policy to restrict operations on packets
    policy0 = controller.get_policy(controller.R_ONLY)
    my_router.add_policy(policy0)
    pkt1 = controller.create_packet('192.168.1.2',
                                    '192.168.1.7',
                                    'AP sure is fun!')
    rule1 = controller.get_forwarding_rule('192.168.1.7',
                                           '192.168.1.14', 7, 0)
    my_router.add_forwarding_rule(rule1)
    try:
        print(parse_route(*my_router.route_packet(pkt1)))
    except customerrors.RoutingException as msg:
        print(msg)

    # Modifying an existing forwarding rule
    rule2 = controller.get_forwarding_rule('192.168.1.7',
                                           '192.168.1.8', 6, 0)
    try:
        my_router.add_forwarding_rule(rule2)
    except customerrors.RoutingException as msg:
        print(msg)

    print(my_router.forwarding_table)
    my_router.update_forwarding_rule(rule2)
    print(my_router.forwarding_table)


def parse_route(pkt, rule):
    """Returns the result of routing a packet in a print-friendly
    format:
    the packet's payload and the forwarding rule used.
    """
    return pkt[indices.PKT_PAYLOAD] + ' ' + str(rule)

if __name__ == '__main__':
    main()
