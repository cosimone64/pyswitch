"""Author: Cosimo Agati

Router module
This module provides a class to represent an abstract router.
Having multple instances of this class makes it possible to control
more than one router.
"""

import collections
import indices
import customerrors


# Constants to ease forwarding rule field access
RULE_NEXT_HOP = 0
RULE_METRIC = 1
RULE_INTERFACE = 2
RULE_IS_USED = 3

# Error messages, placed in constants at the beginning for simplicity
INIT_ERROR_MSG = 'The "interfaces" field in constructor must be a tuple'
EMPTY_TUPLE_MSG = 'At least one interface must be passed in the constructor'
DEST_NOT_FOUND_MSG = 'Packet destination not found in forwarding table'
INACTIVE_RULE_MSG = 'Indexed rule is inactive'
ADD_ERROR_MSG = 'Adding an already present forwarding rule'
UPDATE_ERROR_MSG = 'Updating a nonexistant entry in the forwarsing table'
INTERFACE_ERROR_MSG = ('Interface specified in rule is not present '
                       'in this router')
BAD_RULE_MSG = 'Rule has wrong format'
DROP_ERROR_MSG = 'Dropping a rule that is not in the table'
DELETE_ERROR_MSG = 'Deleting a rule that is not in the table'
UNALLOWED_OP_MSG = 'One or more routing operations are not allowed'
POLICY_ERROR_MSG = 'Argument is not a function'
BLOCKING_RULE_ERROR_MSG = 'Argument is not a function'
PACKET_BLOCKED_MSG = 'Packet blocked'
ADD_OPERATION_MSG = 'Added packet operation is not well formed'


class Router:
    """This class represents an abstract router.
    Interfaces are represented as a tuple, since we assume that a router's
    interfaces cannot be modified.
    The forwarding table is represented as a dictionary, where the index
    is a string containing the destination IP address, and the value
    is a list [next_hop, metric, interface, is_used].
    Policies, blocking rules and operations are represented as lists of
    functions
    """
    def __init__(self, *interfaces):
        if not isinstance(interfaces, tuple):
            raise TypeError(INIT_ERROR_MSG)
        if len(interfaces) < 1:
            raise TypeError(EMPTY_TUPLE_MSG)
        self.interfaces = interfaces
        self.forwarding_table = {}
        self.policies = []
        self.blocking_rules = []
        self.operations = []

    def route_packet(self, pkt):
        """Processes a packet and sends it to destination.
        Before routing, we check if the router's operations
        satisfy its policies, and if the packet must be blocked.
        If everything goes well, all operations are performed on
        the packet. A tuple (new_packet, rule_used) is returned.
        Otherwise, an exception is raised.
        """
        forwarding_rule = self.get_rule_from_table(pkt[indices.PKT_DEST])
        for policy in self.policies:
            for operation in self.operations:
                if not policy(operation):
                    raise customerrors.RoutingException(UNALLOWED_OP_MSG)
        for rule in self.blocking_rules:
            if not rule(pkt):
                raise customerrors.RoutingException(PACKET_BLOCKED_MSG)
        for operation in self.operations:
            operation_function = operation[indices.OP_FUNCTION]
            pkt = operation_function(pkt)
        return (pkt, forwarding_rule)

    def get_rule_from_table(self, rule):
        """Returns the list containing the forwarding rule
        indexed by 'rule' from the forwarding table. If it does
        not exist, or it is marked as False, an exception is raised.
        This method is NOT meant to be used from outside the class.
        """
        if rule not in self.forwarding_table:
            raise customerrors.RoutingException(DEST_NOT_FOUND_MSG)
        if not self.forwarding_table[rule][RULE_IS_USED]:
            raise customerrors.RoutingException(INACTIVE_RULE_MSG)
        return self.forwarding_table[rule]

    def add_forwarding_rule(self, rule):
        """Adds a rule to the forwarding table.
        Will raise an exception if the interface is not present in the
        router.
        """
        rule_dest = get_rule_dest(rule, self.interfaces)
        if rule_dest in self.forwarding_table:
            entry = self.forwarding_table[rule_dest]
            if not entry[RULE_IS_USED]:
                entry[RULE_IS_USED] = True
            else:
                raise customerrors.RoutingException(ADD_ERROR_MSG)
        self.forwarding_table.update(rule)

    def drop_forwarding_rule(self, rule):
        """Drops a forwarding rule, by setting its 'used'
        field to false.
        """
        rule_dest = (rule if isinstance(rule, str)
                     else get_rule_dest(rule, self.interfaces))
        if rule_dest not in self.forwarding_table:
            raise customerrors.RoutingException(DROP_ERROR_MSG)
        dropped_rule = self.forwarding_table[rule_dest]
        dropped_rule[3] = False

    def delete_forwarding_rule(self, rule):
        """Deletes a forwarding rule from the table."""
        rule_dest = (rule if isinstance(rule, str)
                     else get_rule_dest(rule, self.interfaces))
        if rule_dest not in self.forwarding_table:
            raise customerrors.RoutingException(DELETE_ERROR_MSG)
        del self.forwarding_table[rule_dest]

    def update_forwarding_rule(self, rule):
        """Updates an already existing entry in the forwarding
        table. An exception is given if it is not already present.
        """
        rule_dest = get_rule_dest(rule, self.interfaces)
        if rule_dest not in self.forwarding_table:
            raise customerrors.RoutingException(UPDATE_ERROR_MSG)
        self.forwarding_table.update(rule)

    def add_policy(self, policy):
        """Adds a policy to the router's operations."""
        if not isinstance(policy, collections.Callable):
            raise TypeError(POLICY_ERROR_MSG)
        self.policies.append(policy)

    def add_blocking_rule(self, rule):
        """Adds a blocking rule to the router's operations."""
        if not isinstance(rule, collections.Callable):
            raise TypeError(BLOCKING_RULE_ERROR_MSG)
        self.blocking_rules.append(rule)

    def add_operation(self, operation):
        """Adds an operation to be performed to packets."""
        if not (isinstance(operation, dict)
                and indices.OP_TYPE in operation
                and indices.OP_FUNCTION in operation):
            raise TypeError(ADD_OPERATION_MSG)
        self.operations.append(operation)


def get_rule_dest(rule, interfaces):
    """Checks if the rule is valid (its interface is in
    the 'interfaces' list passed, and returns the dest field.
    """
    try:
        rule_dest = list(rule.keys())[0]
        rule_interface = list(rule.values())[0][2]
    except IndexError:
        raise Exception(BAD_RULE_MSG)
    if rule_interface not in interfaces:
        raise Exception(INTERFACE_ERROR_MSG)
    return rule_dest
