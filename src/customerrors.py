"""Author: Cosimo Agati

This module contains custom exceptions for handling errors.
"""


class RoutingException(Exception):
    """This class defines a custom exception
    for routing errors.
    """
    pass


class BadCommandException(Exception):
    """This class defines an exception for
    misformed controller requests.
    """
    pass
